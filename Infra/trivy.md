# Trivy

O [Trivy](https://aquasecurity.github.io/trivy) é uma ferramentas super completa para scanner de recursos de infra e é um projeto Open Source.

[Instalação do trivy](https://aquasecurity.github.io/trivy/v0.37/getting-started/installation/)

```bash
# Dependencias necessárias
sudo apt-get install wget apt-transport-https gnupg lsb-release
# add key
wget -qO - https://aquasecurity.github.io/trivy-repo/deb/public.key | gpg --dearmor | sudo tee /usr/share/keyrings/trivy.gpg > /dev/null
# add repo
echo "deb [signed-by=/usr/share/keyrings/trivy.gpg] https://aquasecurity.github.io/trivy-repo/deb $(lsb_release -sc) main" | sudo tee -a /etc/apt/sources.list.d/trivy.list
# Install
sudo apt-get update
sudo apt-get install trivy
```

## Fonte de dados

O trivy baixa o banco de dados para conferencia de vulnerabilidade a cada 6 horas.

>A Trivy consome apenas recomendações de segurança das fontes abaixo

Quanto aos pacotes instalados a partir de gerenciadores de pacotes do sistema operacional ( dpkg, yum, apketc.), o Trivy usa o banco de dados consultivo do fornecedor do SO.

Por exemplo: para um pacote python instalado de yum(Amazon linux), o Trivy obterá apenas avisos de [ALAS][amazon2]. Mas para um pacote python instalado de outra fonte (por exemplo pip), Trivy obterá avisos dos bancos de dados GitLabe GitHub.

Essa seleção consultiva é essencial para evitar a obtenção de falsos positivos, porque os fornecedores de SO geralmente suportam correções upstream, e a versão corrigida pode ser diferente da versão corrigida upstream. A gravidade é da fonte de dados selecionada. Se a fonte de dados não fornecer gravidade, ela retornará ao NVD e, se o NVD não tiver gravidade, será DESCONHECIDO.

A fonte e tudo que ele scaneia está [aqui](https://aquasecurity.github.io/trivy/v0.37/docs/vulnerability/detection/data-source/):

Sistema Operationais:

- Arch Linux: Vulnerable Issues
- Alpine Linux: secdb
- Wolfi Linux: secdb
- Amazon Linux: Amazon Linux Security Center
- Debian: Security Bug Tracker
- OVAL
- Ubuntu: Ubuntu CVE Tracker
- RHEL/CentOS: OVAL
- Security Data 
- AlmaLinux	AlmaLinux Product Errata
- Rocky Linux: Rocky Linux UpdateInfo
- Oracle Linux: OVAL
- CBL-Mariner: OVAL
- OpenSUSE/SLES: CVRF
- Photon OS: Photon Security Advisory

Linguagens:

- PHP: PHP Security Advisories Database
- GitHub Advisory Database (Composer)
- Python: GitHub Advisory Database (pip
- Open Source Vulnerabilities (PyPI)
- Ruby: Ruby Advisory Database
- GitHub Advisory Database (RubyGems)
- Node.js: Ecosystem Security Working Group
- GitHub Advisory Database (npm)
- Java: GitLab Advisories Community
- GitHub Advisory Database (Maven)
- Go: GitLab Advisories Community
- The Go Vulnerability Database
- Rust: Open Source Vulnerabilities (crates.io)
- .NET: GitHub Advisory Database (NuGet)
- C/C++: GitLab Advisories Community
- Dart: GitHub Advisory Database
- Elixir: GitHub Advisory Database

## Integração

O trivy é somente um binário e pode ser [integrado](https://aquasecurity.github.io/trivy/v0.37/ecosystem/) a várias ferramentas:





## Uso geral

trivy <target> [--scanners <scanner1,scanner2>] <subject>
Exemplos:


trivy image python:3.4-alpine
Resultado

trivy fs --scanners vuln,secret,config myproject/
Resultado

trivy k8s --report summary cluster