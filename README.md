# study-vulnerabilies

Para nos ajudar a encontrar vulnerabilidades podemos ter diferentes ferramentas e precisamos separar em dois tipos:

- Vulnerabilidades a nível de infra
- Vulnerabilidades a nível de código

Geralmente essas ferramentas podem ser integradas em um pipeline para fazer o scaner e reprove ou não uma merge. Geralmente atuam na parte de CI de um pipeline.

## Vulnerabilidade de infra

As vulnerabilidade de infra pode estar em:

- Imagem de container
- Sistema de arquivo
- Repositório git
- Imagem de uma máquina virtual
- Kubernetes
- Cloud

Para isso podemos utilizar diferentes ferramentas que nos ajudam a scanear essas recursos e encontrar:

- Pacotes de SO e dependência de software
- Vulnerabilidades conhecidas (CVE - Common Vulnerabilities and Exposures)
- Problemas de IaC e configurações incorretas
- Informações confidenciais e secrets que não deveriam estar hardcoded
- Licenças de software

Algumas ferramentas serão tratadas em [Infra](./Infra/)

## Vulnerabilidade de código

Ainda existem outros níveis de scanner de código que seriam para scanear um código fonte em busca de:

- Qualidade de código 
- Bibliotecas com vulnerabilidades
- Recursos depreciados

Algumas ferramentas serão tratadas em [Code](./Code/)
